# Situationist-International-Texts

German [copies](https://web.archive.org/web/20091027001822/http://www.geocities.com/situ1968/) of books around the Situationist International.

## Setup

- install [Groff](https://www.gnu.org/software/groff/)
- use [`install-font.sh`](http://www.schaffter.ca/mom/mom-05.html#install-font) to install the [Optima fonts](https://github.com/wsshin/website/tree/master/fonts) (adhere to Groff's [naming conventions](https://www.schaffter.ca/mom/momdoc/appendices.html), e. g. `OptimaBI`)
- install [Pandoc](https://pandoc.org/)
- (also uses `wget` and `sed`)

## Usage

- `mkbook.sh <book> <papersize>`
- `mkallbooks.sh <papersize>`
- `<papersize>` may be one of: `a4`, `a5`
- `<book>` may be one of (in theory, not all work):
  - `spektakel`
  - `handbuch`
  - `leben`
  - `streik`
  - `ratschlaegefuerdiezivilisierten`
  - `lueste`
  - `studentenmilieu`
  - `mai68`
  - `parismai68`
  - `rapport`
  - `kommentare`
  - `publizitaet`
  - `elend`
  - `jetzt`
  - `bibliographie`
  - `students`

## Possible manual improvements
- adjust source html
  - add missing space to chapter titles
  - remove javascript code at bottom
  - remove table of contents then add pandoc option `--toc`
- support other formats; Pandoc's default `ms` template available [here](https://github.com/jgm/pandoc-templates/blob/master/default.ms)