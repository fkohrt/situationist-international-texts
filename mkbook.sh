#!/bin/bash

wget https://web.archive.org/web/20091027095812id_/http://www.geocities.com/situ1968/$1.html
mv $1.html $1-$2.html
tr '\r' ' ' < "$1-$2.html" > "$1-$2_2.html"
tr '\n' ' ' < "$1-$2_2.html" > "$1-$2_3.html"
sed -e 's/ &quot;/ \&bdquo;/g' -e 's/(&quot;/(\&bdquo;/g' -e 's/>&quot;/>\&bdquo;/g' -e 's/&quot; /\&ldquo; /g' -e 's/&quot;)/\&ldquo;)/g' -e 's/&quot;,/\&ldquo;,/g' -e 's/&quot;\./\&ldquo;\./g' -e 's/&quot;!/\&ldquo;!/g' -e 's/&quot;?/\&ldquo;?/g' -e 's/&quot;;/\&ldquo;;/g' -e 's/&quot;:/\&ldquo;:/g' -e 's/&quot;</\&ldquo;</g' -e 's/ - / \&ndash; /g' -e 's/ -,/ \&ndash;,/g' -e 's/-, /\&ndash;, /g' "$1-$2_3.html" > "$1-$2_4.html"
# manual possible imporvements:
# * add missing space to chapter titles
# * remove javascript code at bottom
# * remove the toc, then add pandoc option `--toc`
pandoc --data-dir="./pandoc-data/$2" --to ms -s -V lang="de" -V fontfamily="Optima" -V pointsize="12.5p" -V lineheight="15p" -V papersize="$2" --pdf-engine-opt="-dpaper=$2" --pdf-engine-opt="-P-p$2" -o "$1-$2.pdf" "$1-$2_4.html"
rm $1-$2.html
rm $1-$2_2.html
rm $1-$2_3.html
rm $1-$2_4.html
